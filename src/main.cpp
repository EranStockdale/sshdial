#if defined(unix) || defined(__unix__) || defined(__unix)
# define PREDEF_PLATFORM_UNIX
#endif

#if defined(PREDEF_PLATFORM_UNIX) || defined(__linux__) || defined(__APPLE)
# define PREDEF_PLATFORM_UNIX_LIKE
#endif

#if defined(_WIN32) || defined(_WIN64)
# define PREDEF_PLATFORM_WINDOWS
#endif

#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <filesystem>
#ifdef PREDEF_PLATFORM_UNIX_LIKE
#include <pwd.h>
#include <unistd.h>
#endif // PREDEF_PLATFORM_UNIX_LIKE
#include <vector>
#include <map>

namespace fs = std::filesystem;

std::string stringify(std::vector<std::string> vec) {
	std::string output = "(";
	for (int i = 0; i < vec.size() - 1; i++) {
		output += vec[i] + ", ";
	}
	output += vec.back() + ")";
	return output;
}

// thanks for the help, https://stackoverflow.com/questions/2552416/how-can-i-find-the-users-home-dir-in-a-cross-platform-manner-using-c
std::string getHome() {
	std::string homeDirectory = "NULL";
#if defined(_WIN32) || defined(_WIN64)
	if (const char* userprofile = std::getenv("USERPROFILE")) {
		homeDirectory = userprofile;
	} else {
		std::string homeDrive = std::getenv("HOMEDRIVE");
		std::string homePath = std::getenv("HOMEPATH");
		if (homeDrive.empty() || homePath.empty()) {
			return "";
		}

		homeDirectory = (fs::path(homeDrive) / fs::path(homePath)).string();
	}
#elif defined(PREDEF_PLATFORM_UNIX_LIKE)
	struct passwd* pwd = getpwuid(getuid());
	if (pwd) {
		homeDirectory = pwd->pw_dir;
	} else {
		homeDirectory = std::getenv("HOME");
	}
#else
#error Unsupported platform!
#endif
	return homeDirectory;
}

struct Host {
	std::vector<std::string> names;
	std::map<std::string, std::string> properties;
};

int main() {
	std::string homeDirectory = getHome();
	if (homeDirectory.empty()) {
		printf("Unable to find home directory!\n");
		return 1;
	}

    fs::path configPath = fs::path(homeDirectory) / fs::path(".ssh") / fs::path("config");
	std::ifstream f = std::ifstream(configPath.string());
	if (!f) {
		printf("SSH config file does not exist.\n");
		return 1;
	}

	std::vector<Host> hosts;
	int lineNumber = 0;
	std::string line;
	while (std::getline(f, line)) {
		printf("Line %i: %s\n", lineNumber, line.c_str());
		if (line.rfind("Host", 0) == 0) {
			printf("found new host on line %i\n", lineNumber);
			hosts.push_back({});
			
			std::istringstream iss(line.substr(4));
			std::string hostname;
			while (iss >> hostname) {
				hosts.back().names.push_back(hostname);
			}
			printf("Hostnames: %s\n", stringify(hosts.back().names).c_str());
		} else if (line.empty()) {
			// do nothing.
		} else {
			std::istringstream iss(line);
			// this is probably more than mildly unsafe
			std::string property; iss >> property;
			std::string value   ; iss >> value   ;
			hosts.back().properties.insert({property, value});
		}

		lineNumber++;
	}

	printf("Loaded %i hosts!\n", hosts.size());
	for (Host host : hosts) {
		printf("Host %s has %i properties.\n", stringify(host.names).c_str(), host.properties.size());
		for (auto const& x : host.properties) {
			printf("\t%s: %s\n", x.first.c_str(), x.second.c_str());
		}
	}
	return 0;
}